import React from 'react';
//identify the components and packages that we are going to use to build this component.

import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
//we are going to build this with a jumbotron from bootstrap.

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
//we will also get the bootstrap grid system (named exports).

//lets create a function that will return/render our component
//variant is a prop from react-boostrap which describes the style type.
export default function Banner() {
  return (

    <Row>
      <Col>
        <Jumbotron>
          <h1> Culinary School </h1>
          <p>Become a Master Chef</p>
          <Button variant="success">Enroll Now!</Button>
        </Jumbotron>
      </Col>
    </Row>
  )
}