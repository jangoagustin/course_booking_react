import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

//lets acquire proptypes from react. this is a built in feature/function in react.
import PropTypes from 'prop-types';

export default function Course({course}) {
  //we will destructure the course prop into its properties, while describing the properties that we want to take from the object.
  const { name, description, price } = course;

  return (
    <div className="mt-4">
      <Card>
        <Card.Body>
          <Card.Title> {name} </Card.Title>
            {/* <Card.Subtitle>₱ 3000</Card.Subtitle> */}
            <Card.Text>
              <span className="subtitle"> Description: </span>
              <br/>
              {description} 
              <br/>
              <span className="subtitle"> Price: </span>
              <br/>
              {price}
              <br/>
              {/* This will describe the number of enrolled students in a batch */}
              <span className="subtitle"> Enrollees: </span>
              <br/>
              <br/>
              {/* This will describe the number of seats available in a class */}
              <span className="subtitle"> Seats: </span>
              <br/>
              <br/>
            </Card.Text>
            <Button variant="success">Enroll</Button>
        </Card.Body>
      </Card>
    </div>
  )
}

//lets use the proptypes to identify/describe the proper data type for the data of courses.
Course.propTypes = {
  //shape() is used to check that a property of an object conforms to a specific shape
  course: PropTypes.shape({
    //define the properties and their expected data types.
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }) 
}