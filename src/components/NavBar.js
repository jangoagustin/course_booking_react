import React from 'react';
//we will now try to create our first react component, we first have to identify the necessary dependencies/packages needed.
//import bootstrap components to create the navbar.
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

//two kidns of exports
//=> default export -> these components/modules can be repackaged inside another variable but you have to retrieve them using the exact path/location.
//=> named export -> these are identified with "{}" , but they cannot be repackaged.

//we try to build now the navbar component using bootstrap and we will render it inside the browser.
export default function NavBar() {
  return(
    <Navbar bg="light" expand="lg">
      <Navbar.Brand>React Course Booking 87</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#"> Home </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}