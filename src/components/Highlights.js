import React from 'react';
//identify the needed components to build this new module

//we are going to use the card component from react-bootstrap.
import Card from 'react-bootstrap/Card';

//acquire the bootstrap grid system for formatting purposes.
import { Row, Col } from 'react-bootstrap';
//you can use double if you use the named export syntax. this will not work with default export because that uses a specific path.

//lets create a function that will render our new component.
export default function Highlights() {
  return(
    <Row>
      {/* First Card */}
      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Img variant="top" src="https://images.pexels.com/photos/6249501/pexels-photo-6249501.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" />
          <Card.Body>
            <Card.Title>Sushi Master Course</Card.Title>
            <Card.Text>
              Gaff Davy Jones' Locker doubloon fire in the hole sutler swab scurvy brig Corsair loot. Brethren of the Coast stern walk the plank sloop yo-ho-ho brig bucko pirate quarterdeck long clothes. Black spot rum aye furl rigging holystone broadside chase guns grapple yo-ho-ho.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      {/* Second Card */}
      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Img variant="top" src="https://images.pexels.com/photos/725997/pexels-photo-725997.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" />
          <Card.Body>
            <Card.Title>Pasta Making</Card.Title>
            <Card.Text>
              Gaff Davy Jones' Locker doubloon fire in the hole sutler swab scurvy brig Corsair loot. Brethren of the Coast stern walk the plank sloop yo-ho-ho brig bucko pirate quarterdeck long clothes. Black spot rum aye furl rigging holystone broadside chase guns grapple yo-ho-ho.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      {/* Third Card */}
      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Img variant="top" src="https://images.pexels.com/photos/1105325/bbq-meet-eating-diner-1105325.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" />
          <Card.Body>
            <Card.Title>Flippin' n' Grillin'</Card.Title>
            <Card.Text>
              Gaff Davy Jones' Locker doubloon fire in the hole sutler swab scurvy brig Corsair loot. Brethren of the Coast stern walk the plank sloop yo-ho-ho brig bucko pirate quarterdeck long clothes. Black spot rum aye furl rigging holystone broadside chase guns grapple yo-ho-ho.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}