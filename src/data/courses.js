export default [ //this will serve as the module that holds the records about the courses in our booking system.
  {
    id: "wdc001",
    name: "Sushi Master Course",
    description: "Loot take a caulk quarterdeck cog Arr grog blossom heave down rigging gun parrel. Draught hogshead red ensign heave down rope's end flogging Barbary Coast handsomely come about pinnace. Skysail keelhaul take a caulk case shot mutiny tack pirate to go on account salmagundi barkadeer.",
    price: 45000, //numeric
    onOffer: true //will describe is the course is being offered
  },
  {
    id: "wdc002",
    name: "Pasta Making",
    description: "Square-rigged pirate jury mast starboard pinnace Shiver me timbers bilge tack lad bounty. Bring a spring upon her cable transom tender plunder hang the jib dance the hempen jig no prey, no pay brigantine schooner mizzenmast. Pink shrouds scallywag Sail ho deadlights sloop galleon long clothes take a caulk skysail.",
    price: 55000, //numeric
    onOffer: true //will describe is the course is being offered
  },
  {
    id: "wdc003",
    name: "Flippin' n' Grillin'",
    description: "Coffer lee hogshead ballast topsail log barque loot rope's end bilge water. Clap of thunder Privateer boom swab mizzen pink pillage pinnace Pieces of Eight lanyard. Wench tackle cable no prey, no pay skysail Letter of Marque starboard schooner stern chandler.",
    price: 60000, //numeric
    onOffer: true //will describe is the course is being offered
  }
]