import React from 'react';
//we will also render the course component inside this page.
import Course from './../components/Course'; //this will serve as the container for our data/records for the number of courses available.
import coursesData from './../data/courses'; //this describes the records

export default function Courses() {
  //next task is to create multiple courses components to the content/records of the coursesData variable.
  const courseContent = coursesData.map(course => {
    return (
      <Course key={course.id} course={course}/>
    )
  })
  return(
    <div>
      {courseContent}
    </div>
  )
}
