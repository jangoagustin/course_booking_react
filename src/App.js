import './App.css';
import NavBar from './components/NavBar';
// import Home from './pages/Home';
import Courses from './pages/Courses';
import { Container } from 'react-bootstrap';

export default function App() {
  return (
    <div>
      <NavBar />
      <Container>
        {/* <Home /> */}
        <Courses/>
      </Container>
    </div>
  );
}


//task:
//create a project in your gitlab archives in batch 87
//project name: react_course_booking
//description: this is the discussion session project for react
//commit message: done installing the initial dependencies
//push.